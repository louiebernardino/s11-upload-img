import React, { useState, useEffect } from 'react'
import { Button, Modal, ModalHeader, ModalBody, Form, FormGroup, Label, Input } from 'reactstrap'

const MemberModal = ({modal, toggle, member, updateMember, teams}) => {
	const [memberModal, setMemberModel] = useState({
		teamId: "",
		position: "",
		teams: []
	})

	const { teamId, position} = memberModal

	useEffect(() => {
		setMemberModel({
			teamId: member.teamId ? member.teamId._id : null,
			position: member.position ? member.position : null,
			teams: teams
		})
	}, [toggle])

	//POPULATE DROPDOWN
	const populateTeams = () => {
		return teams.map(team => {
			return (
				<option key ={team._id} value={team._id} selected={ teamId === team._id ? true : false}>
					{team.name}
				</option>

				)
		})
	}

	//IF TEAMID IS NULL
	let na
	if (teamId == null || position == null) {
		na = (
			<option selected disabled>Please choose one...</option>
		)
	}

	//HANDLE CHANGES TO FORM
	const onChangeHandler = (e) => {
		console.log({[e.target.name] : e.target.value})
		setMemberModel({
			...memberModal,
			[e.target.name] : e.target.value //element
		})
	}

	//HANDLE SUBMIT
	const onSubmitHandler = (e) => {
		e.preventDefault() //prevents the page from reloading

		const updates = {
			teamId,
			position
		}
		console.log(updates)

		updateMember(member._id, updates)
	}

	return (
		<Modal isOpen={modal} toggle={toggle}>
			<ModalHeader toggle={toggle}>Update Member</ModalHeader>
			<ModalBody>
				<Form className="border rounded" onSubmit={e => onSubmitHandler(e)}>
					<FormGroup>
						<Label for="username">Username</Label>
						<Input type="text" name="username" id="username" value={member.username} disabled/>
					</FormGroup>
					<FormGroup>
						<Label for="teamId">Team</Label>
						<Input type="select" name="teamId" id="teamId" onChange={e => onChangeHandler(e)}>
						{ na }
						{populateTeams()}
						</Input>
					</FormGroup>
					<FormGroup>
						<Label for="position">Position</Label>
						<Input type="select" name="position" id="position" onChange={e => onChangeHandler(e)}>
							{ na }
							<option value="admin" 		selected={ member.position === "admin" ? true : false}>Admin</option>
							<option value="ca" 			selected={ member.position === "ca" ? true : false}>CA</option>
							<option value="student"	 	selected={ member.position === "student" ? true : false}>Student</option>
							<option value="hr" 			selected={ member.position === "hr" ? true : false}>HR</option>
							<option value="instructor" 	selected={ member.position === "instructor" ? true : false}>Instructor</option>
						</Input>
					</FormGroup>
					<Button>Save Changes</Button>
				</Form>
			</ModalBody>
		</Modal>



		)
}

export default MemberModal