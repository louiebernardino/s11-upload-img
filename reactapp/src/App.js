import React, {Fragment, useState} from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch, Redirect} from 'react-router-dom'
import { Button } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import "./style.css";

//pages
import AppNavbar from './partials/AppNavbar';
import MembersPage from './pages/MembersPage';
import LandingPage from './pages/LandingPage';
import TeamsPage from './pages/TeamsPage';
import TasksPage from './pages/TasksPage';
import NotFoundPage from './pages/NotFoundPage';
import LoginPage from './pages/LoginPage';
import RegisterPage from './pages/RegisterPage';
import ProfilePage from './pages/ProfilePage';
import MembersProfilePage from './pages/MembersProfilePage'
import TasksProfilePage from './pages/TasksProfilePage'


const root = document.querySelector("#root")
	//<Fragment>
		//<h1 className="bg-pink">Hello, Batch 44!</h1>
		//<button className="btn btn-danger">bootstrap button</button>
		//<Button color="primary" className="ml-3">Reactstrap Button</Button>
	//</Fragment>

const App = () => {

	const [appData, setAppData] = useState({
		token: localStorage.token,
		username: localStorage.username
	})

	const { token, username} = appData //object destructuring
	// console.log(token)
	console.log("App username", username)

	//for putting username to memberspage
	const getCurrentMember = () => {
		return { username, token }
	}

	//render - accepts a function and returns a component
	// component -> props -> routes (we need to address the component of the route)


	// <MembersPage username={ username } token={ token }/>
	const Load = (props, page) => {

		if( token === null) return <Redirect to="/login" />

		if(page === "LogoutPage") {
			localStorage.clear()
			setAppData({
				token,
				username
			})
			return window.location = "/login"
		}


		switch(page) {
			case "MembersPage": return <MembersPage{...props} tokenAttr={ token }/>
			case "MembersProfilePage": return <MembersProfilePage{...props} tokenAttr={ token }/>
			case "TasksProfilePage": return <TasksProfilePage{...props} tokenAttr={ token }/>			
			case "ProfilePage": return <ProfilePage {...props} tokenAttr={token}/>
			case "TeamsPage": return <TeamsPage{...props} tokenAttr={ token }/>
			case "TasksPage": return <TasksPage{...props} tokenAttr={ token }/>
			default: return <NotFoundPage/>	
		}
	}

	return (
	<BrowserRouter>
		<AppNavbar token={ token } usernameAttr={ username } getCurrentMemberAttr={ getCurrentMember() }/>
		<Switch>
			<Route path="/members/:id" render={ (props)=> Load(props, "MembersProfilePage")}/>
			<Route path="/tasks/:id" render={ (props)=> Load(props, "TasksProfilePage")}/>
			<Route path="/members" render={ (props)=> Load(props, "MembersPage")}/>
			<Route component = {LandingPage} exact path="/"/>
			<Route path="/teams" render={(props) => Load(props, "TeamsPage")}/>
			<Route path="/tasks" render={(props) => Load(props, "TasksPage")}/>
			<Route component = { LoginPage } exact path="/login"/>
			<Route path="/profile" render={(props)=> Load(props, "ProfilePage")}/>
			<Route component = { RegisterPage } exact path="/register"/>
			<Route path="/logout" render={(props) => Load(props, "LogoutPage")}/>
			<Route path="*" render={(props)=> Load(props, "NotFoundPage")}/>
		</Switch>
	</BrowserRouter>
	) 
}

export default App
