//Dependencies
import React, {useState, useEffect, Fragment} from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import MemberForm from '../forms/MemberForm';
import MemberTable from '../tables/MemberTable';
import MemberModal from '../modals/MemberModal';
import axios from 'axios';


//COMPONENTS
const MembersPage = (props) => { //props - arguments
/*anonymous function*/
//JSX
console.log("memberpage props", props.tokenAttr)

const [ membersData, setMembersData] = useState({
  token: props.tokenAttr,
  members: []
})

const { token, members} = membersData
const config = {
  headers: {
    Authorization: `Bearer ${token}`
  }
}

const url = "http://localhost:4051"

console.log("MembersPage Token", token)
console.log("MembersPage members", members)

//access members from nodeapp
const getMembers = async (query = "") => {
  try {

    const res = await axios.get(`${url}/members${query}`, config)

    console.log("Members Res", res)
    return setMembersData({
      ...membersData,
      members: res.data
    })

  } catch(e) {
    console.log(e.response)
    //swal
  }
}

//deleting a member by an admin
const deleteMember = async (id) => {
  try {

    const res = await axios.delete(`${url}/members/${id}`, config)
    console.log("Delete testing", res.data)
    getMembers()
  } catch(e) {
    //SWAL
    console.log(e)
  }
}

useEffect(()=> {
  getMembers()
}, [setMembersData])

//Update Member: Modal
//declare state
const [modalData, setModalData] = useState({
  modal: false,
  member: {}
}) 

const { modal, member } = modalData

const toggle = async (id) => {

try {
  if (typeof id === 'string') {
    const res = await axios.get(`${url}/members/${id}`, config)
    console.log(res)

    return setModalData({
      modal:!modal,
      member: res.data
    })
  }
  setModalData({
      ...modalData,
      modal: !modal
    })
  } catch(e) {
    console.log(e.response)
  }
}

//GET AL TEAMS
const [teams, setTeams] = useState([]) //teams contains arrays

const getTeams = async () => {
  try {
      const res = await axios.get(`${url}/teams`, config)
      setTeams(res.data) //config contains token
  } catch(e) {
      console.log(e)
  }
}

//used to stop infinite looping and displaying data
useEffect(() => {
  getTeams()
}, [setTeams]) //binabantayan niya si setTeams

//UPDATE MEMBER
const updateMember = async (id, updates) => {
    console.log("UPDATE MEMBER************", id, updates)
    try {
      config.headers["Content-Type"] = "application/json"

      const body = JSON.stringify(updates)
      const res = await axios.patch(`${url}/members/${id}`, body, config)

      setModalData({
        ...modalData,
        modal:!modal
      })
      getMembers()
      //SWAL
      
    } catch(e) {
      console.log(e)

    }
}



  return (
  <Fragment>
    <Container>
      <Row className="mb-4">
        <Col>
          <h1>Members Page</h1>
        </Col>
      </Row>
      <Row>
        <Col md="4"><MemberForm/></Col>
        <Col>
        <div>
          <Button className="btn-sm border mr-1" onClick={()=> getMembers()}>Get All</Button>
          <Button className="btn-sm border mr-1" onClick={()=> getMembers("?isActive=true")}>Get Active</Button>
          <Button className="btn-sm border mr-1" onClick={()=> getMembers("?isActive=false")}>Get Deactivated</Button>
        </div>
        <hr/>
        <MemberTable membersAttr={members} deleteMember={deleteMember} toggle={toggle}/></Col>
      </Row>
    </Container>
    <MemberModal toggle={toggle} modal={modal} member={member} teams={teams} updateMember={updateMember}/>
  </Fragment>
  );
}

export default MembersPage;

//Discussion
//1.) What is a component?
// - function or class.
// - contains props (arguments)