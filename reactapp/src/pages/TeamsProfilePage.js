import React, {useState, useEffect} from 'react'
import { Container, Row, Col } from 'reactstrap'
import TeamsProfileImgForm from '../forms/TeamsProfileImgForm'
import axios from 'axios';

const TeamsProfilePage = (props) => {

// console.log("ProfilePage", props.getCurrentMemberAttr)

	//STATE
	const [teamsProfileData, setTeamsProfileData] = useState({})

	const config = {
		headers: {
			Authorization: `Bearer ${props.tokenAttr}`
		}
	}

	const url = "http://localhost:4051/members"
	//getMember
	const getMember = async() => {
		try {
			const res = await axios.get(`${url}/me`, config)
			console.log("Profile Page Res Data", res.data)
			const member = res.data
			setProfileData({
				_id: member._id,
				firstName: member.firstName,
				lastName: member.lastName,
				position: member.position,
				username: member.username
			})
		} catch(e) {
			console.log(e)
		}
	}

	useEffect(()=> {
		getMember()
	}, [])

	//UpdateMember

	//UpdateImage
	const updateImage = async (body) => {
		try {
			console.log(body)

			const res = await axios.post(`${url}/upload`, body, config)
			//SWALL
			window.location.reload()
		} catch(e) {
			//SWAL
			console.log(e)
		}
	}

	return (
		<Container className = "my-5">
			<Row className = "mb-3">
				<Col>
					<h1>Profile Page!</h1>
				</Col>
			</Row>
			<Row className = "mb-3">
				<Col md="6">
					<ProfileImgForm _id={profileData._id} updateImage={updateImage}/>
				</Col>
			</Row>
		</Container>
	)
}

export default TeamsProfilePage;