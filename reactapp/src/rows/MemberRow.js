import React, {Fragment} from 'react';
import { Table } from 'reactstrap';
import { Container, Row, Col } from 'reactstrap';
import { Button } from 'reactstrap';
import {Link} from 'react-router-dom'


const MemberRow = ({memberAttr, index, deleteMember, toggle}) => {
const { firstName, lastName, username, email, _id, teamId, isActive } = memberAttr
let deleteBtn = ""

  if(isActive) {
    deleteBtn = (
      <Button color="danger" onClick={()=> deleteMember(_id)}><i className="fas fa-trash-alt"></i></Button>
    )
  }

  return (
  	<Fragment>
  		<tr>
          <th scope="row">{ index }</th>
          <td>{ username }</td>
          <td>{ teamId ? teamId.name : "N/A" }</td>
          <td>Team 1</td>
          <td>Admin</td>
          <td>{isActive ? "Active": "Deactivated"}</td>
          <td>
          <Link size="sm" className="btn btn-info" to={`/members/${_id}`}><i className="fas fa-eye"></i></Link>
           <Button color="warning" onClick={()=> toggle(_id)}><i className="fas fa-edit"></i></Button>
          { deleteBtn }
          </td>
      </tr>
    </Fragment>
	);
}

export default MemberRow;

