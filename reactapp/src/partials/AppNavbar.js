import React, { useState, Fragment } from 'react';
import { Link } from 'react-router-dom';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from 'reactstrap';
import { Button } from 'reactstrap';

const AppNavbar = (props) => {
  console.log("AppNavbar props", props.token)
  console.log("AppNavbar props username", props.username)
  console.log("AppNavbar props getCurrentMemberAttr", props.getCurrentMemberAttr.username)

  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  let authLinks = "";
  let authLinks2 = "";

  if(!props.token) {
    authLinks = (
      <Fragment>
          <NavItem><Link to="/login" className="nav-link">Login</Link></NavItem>
          <NavItem><Link to="/register" className="nav-link">Register</Link></NavItem>
      </Fragment>
      )
  } else {
    authLinks = (
      <Fragment>
          <NavItem><Link to="/profile" className="nav-link">Welcome, { props.usernameAttr  }</Link></NavItem>
          <NavItem><Link to="/logout" className="nav-link">Logout</Link></NavItem>
      </Fragment>

      )
  }

  if(!props.token) {
  } else {
    authLinks2 = (
      <Fragment>
          <NavItem className="text-black"><Link to="/members" className="nav-link">Members</Link></NavItem>
          <NavItem><Link to="/teams" className="nav-link">Teams</Link></NavItem>
          <NavItem><Link to="/tasks" className="nav-link">Tasks</Link></NavItem> 
      </Fragment>

      )
  }

  return (
    <div className="mb-5">
      <Navbar color="secondary" light expand="md">
        <NavbarBrand className="font-weight-bold text-white" href="/">MERN TRACKER</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            {authLinks2}
          </Nav>
          <Nav navbar>
            {authLinks}
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
}

export default AppNavbar;