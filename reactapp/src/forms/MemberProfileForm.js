import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

const MemberProfileForm = (props, {memberAttr}) => {

  const { firstName, lastName, username, email, teamId, position} = props.member

  const [ setMember, setMemberData ] = useState({
    teams: [],
    teamId2: "",
    position2: ""
  })

  const {teams, teamId2, position2} = setMember

  useEffect(()=> {
    setMemberData({
      teams: props.teams,
      teamId: teamId ? teamId._id : null,
      position2: position ? position: null
    })
  }, [props])

  //POPULATE
  const populateTeams = () => {
    return teams.map(team => {
      return (
          <option
          key={team._id}
          value={team._id}
          selected={ teamId2 === null ? false : teamId2 === team._id ? true : false}
          >{team.name}</option>
        )
    })
  }

  return (
    <Form className="m-3">
      <FormGroup>
        <Label for="firstName">First Name</Label>
        <Input type="text" name="firstName" id="firstName" value={firstName}/>
      </FormGroup>
      <FormGroup>
        <Label for="lastName">Last Name</Label>
        <Input type="text" name="lastName" id="lastName" value={lastName}/>
      </FormGroup>
      <FormGroup>
        <Label for="username">Username</Label>
        <Input type="text" name="username" id="username" value={username}>
        </Input>
      </FormGroup>
      <FormGroup>
        <Label for="email">Email</Label>
        <Input type="email" name="email" id="email" value={email}>
        </Input>
      </FormGroup>
      <FormGroup>
        <Label for="teamId2">Team</Label>
        <Input type="select" name="teamId2" id="teamId2" value={teamId2}>
          {populateTeams()}
        </Input>
      </FormGroup>
      <Button color="primary" size="sm" block>Save Changes</Button>
    </Form>
  );
}

export default MemberProfileForm;